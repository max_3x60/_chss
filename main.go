package main

import (
	app "chss/app"
	"chss/app/core"
	"chss/lib/fen"
	"embed"
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"log"
	"time"
)

//go:embed assets
var fs embed.FS

var DEBUG_BENCHMARK = false

func main() {
	if DEBUG_BENCHMARK {
		fmt.Println("Running benchmark...\n")
		fmt.Println("---[ BENCHMARK RESULT ]---", benchmark(3))
	}

	app.GameFontSmall = loadFont(fs, "assets/fonts/NotoSans-Regular.ttf", 16)
	app.GameFontNormal = loadFont(fs, "assets/fonts/NotoSans-Regular.ttf", 21)
	app.GameFontBig = loadFont(fs, "assets/fonts/NotoSans-Bold.ttf", 36)
	app.GameFontGiant = loadFont(fs, "assets/fonts/NotoSans-Bold.ttf", 64)

	game := app.NewGame(1920, 1080, fs)

	ebiten.SetWindowSize(game.Width, game.Height)
	ebiten.SetWindowTitle("./CHSS")
	ebiten.SetFPSMode(ebiten.FPSModeVsyncOn)

	if err := ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}

}

func loadFont(fs embed.FS, fontPath string, size float64) font.Face {
	rawFont, err := fs.ReadFile(fontPath)
	if err != nil {
		log.Fatal(err)
	}
	tt, err := opentype.Parse(rawFont)
	if err != nil {
		log.Fatal(err)
	}
	f, err := opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    size,
		DPI:     72,
		Hinting: font.HintingFull,
	})
	if err != nil {
		log.Fatal(err)
	}

	return f
}

func benchmark(depth int) string {
	m, _ := core.NewMatch(fen.FENStringDefault, []byte{})

	var cycleStamp = time.Now()
	var result = "\nDepth		Time			Total	"

	for i := 1; i < depth+1; i++ {
		cycleStamp = time.Now()
		tree := core.NewTree(m.Board, fen.PlayerWhite, i)
		calcTime := time.Now().Sub(cycleStamp).Milliseconds()

		result += "\n" + fmt.Sprintf("%d		%dms			%d", i, calcTime, _count(tree.Root.Children, 0))
	}

	return result
}

func _count(itms []*core.TreeNode, cntr int) int {
	for _, child := range itms {
		if !child.IsSane {
			continue
		}

		cntr++

		if !child.IsLeaf() {
			cntr += _count(child.Children, 0)
		}
	}

	return cntr
}
