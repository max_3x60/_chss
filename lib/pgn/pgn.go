package pgn

import (
	"chss/lib/fen"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type Comment struct {
	Source   string
	AfterNum int
}

const LIBPGN_DEBUG = false

type Position [2]rune

//ToXY in a cartesian grid with origin in the top left corner.
func (p Position) ToXY() (int8, int8) {
	x := int8(0)
	if p[0] == 'b' {
		x = 1
	} else if p[0] == 'c' {
		x = 2
	} else if p[0] == 'd' {
		x = 3
	} else if p[0] == 'e' {
		x = 4
	} else if p[0] == 'f' {
		x = 5
	} else if p[0] == 'g' {
		x = 6
	} else if p[0] == 'h' {
		x = 7
	}

	y, _ := strconv.Atoi(string(p[1]))

	return x, 8 - int8(y)
}

type PGN struct {
	Moves    MovePairList
	Comments []*Comment
}

const REGEX_TEST_MOVES = `(?m)([0-9]+?\.[\s]*?[0-9a-zA-Z\-\+]+?[\s\n]+[0-9a-zA-Z\-\+]+)+|({[[:alpha:]\säöü]*})`

// REGEX_TEXT_TURN Split up a single FEN move (14.Bg3 Kc8) into its component parts; respect comments.
const REGEX_TEST_TURN = `([0-9]+?)\.[\s]*?([0-9a-zA-Z\-\+]+?)[\s\n]+([0-9a-zA-Z\-\+]+)|({[[:alpha:]\säöü]+?})`

func From(startPos fen.FEN, match []byte) (PGN, error) {
	result := PGN{}

	recover()
	exp := regexp.MustCompile(REGEX_TEST_MOVES)

	recover()
	moves := exp.FindAll(match, -1)

	if len(moves) > 0 {
		for a := 0; a < len(moves); a++ {
			move := strings.Replace(string(moves[a]), "\n", " ", -1)

			recover()
			expTurn := regexp.MustCompile(REGEX_TEST_TURN)

			turn := expTurn.FindAllSubmatch([]byte(move), -1)[0]

			turnNum, _ := strconv.ParseInt(string(turn[1]), 10, 8)

			if LIBPGN_DEBUG {
				fmt.Println("MOVE: ", move)
				fmt.Println("  [0]: ", string(turn[0]))
				fmt.Println("  [1]: ", string(turn[1]))
				fmt.Println("  [2]: ", string(turn[2]))
				fmt.Println("  [3]: ", string(turn[3]))
				fmt.Println("  [4]: ", string(turn[4]))
			}

			// We have a comment
			if len(turn[4]) > 0 {
				result.Comments = append(result.Comments, &Comment{
					Source:   string(turn[4]),
					AfterNum: len(result.Moves),
				})
			} else {
				pair, _ := MovePairFromString(string(turn[2])+" "+string(turn[3]), int(turnNum))
				result.Moves = append(result.Moves, &pair)

				if LIBPGN_DEBUG {
					fmt.Println("Appended Pair: ", pair)
				}
			}
		}
	}

	return result, nil
}

func (p *PGN) HalveMove(i int) (*Move, error) {
	var HM_MAX = len(p.Moves)*2 - 1

	if HM_MAX >= i {
		if (i%2)+1 <= len(p.Moves[i/2].Moves) {
			return &p.Moves[i/2].Moves[i%2], nil
		}

		return nil, errors.New(fmt.Sprint("MovePair[", i/2, "] doesn't contain Move [", i%2, "]: ", p.Moves[i/2].Moves[i%2]))
	}

	return nil, errors.New(fmt.Sprint("HalveMove [", i, "] out of bound, max is ", HM_MAX))
}

func extractPieceTarget(ply fen.Player, moveString string) (fen.Piece, Position, []rune) {
	if len(moveString) > 0 {

		raw := []rune(moveString)

		// moveString indicates Nil turn.
		if raw[0] == 'O' {
			return fen.NilPiece, Position{}, []rune{}
		}

		// moveString is a simple movement.
		if len(raw) == 2 {
			if ply == fen.PlayerBlack {
				return fen.BlackPawn, [2]rune{raw[0], raw[1]}, []rune{}
			} else {
				return fen.WhitePawn, [2]rune{raw[0], raw[1]}, []rune{}
			}
		} else
		// moveString is a take.
		if len(raw) == 3 {
			return Piece(raw[0]).ToFEN(ply), [2]rune{raw[1], raw[2]}, []rune{}
		} else if len(raw) == 4 && raw[1] == 'x' {
			if strings.ContainsAny(string(raw[0]), "abcdefgh12345678") {
				if ply == fen.PlayerBlack {
					return fen.BlackPawn, [2]rune{raw[2], raw[3]}, []rune{raw[0]}
				} else {
					return fen.WhitePawn, [2]rune{raw[2], raw[3]}, []rune{raw[0]}
				}
			} else {
				return Piece(raw[0]).ToFEN(ply), [2]rune{raw[2], raw[3]}, []rune{}
			}
		} else
		// moveString contains a promotion.
		if strings.Index(moveString, "+") > -1 {
			if len(raw) == 4 {
				return Piece('P').ToFEN(ply), [2]rune{raw[1], raw[2]}, []rune{rune(Piece(raw[0]).ToFEN(ply))}
			} else if len(raw) == 5 {
				return Piece('P').ToFEN(ply), [2]rune{raw[2], raw[3]}, []rune{rune(Piece(raw[0]).ToFEN(ply)), raw[1]}
			}
		} else {
			from := []rune{}
			if strings.ContainsAny(string(raw[1]), "abcdefgh12345678") {
				from = []rune{raw[1]}
			}

			if strings.ContainsAny(string(raw[0]), "abcdefgh12345678") {
				from = []rune{raw[0]}
				if raw[1] == 'x' {
					return Piece('P').ToFEN(ply), [2]rune{raw[2], raw[3]}, from
				} else {
					return Piece('P').ToFEN(ply), [2]rune{raw[1], raw[2]}, from
				}
			} else {
				if raw[2] == 'x' {
					return Piece(raw[0]).ToFEN(ply), [2]rune{raw[3], raw[4]}, from
				} else {
					return Piece(raw[0]).ToFEN(ply), [2]rune{raw[2], raw[3]}, from
				}
			}
		}
	}

	return fen.NilPiece, Position{}, []rune{}
}
