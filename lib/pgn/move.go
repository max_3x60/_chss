package pgn

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type MovePair struct {
	Source string
	Num    int
	Moves  [2]Move
}
type Move struct {
	Piece Piece
	// Target square, relative to a grid starting with 0,0 in the top left corner.
	Target []int32
	// Unique may be filled if additional info was given.
	Unique              []rune
	IsTaking            bool
	IsPromoting         bool
	IsCastlingKingSide  bool
	IsCastlingQueenSide bool
	// IsScoreCounter will be true if this is the final move of the match.
	// score is stored in Move.Unique
	IsScoreCounter bool
}

func (m *Move) IsCastling() bool {
	return m.IsCastlingKingSide || m.IsCastlingQueenSide
}

func (m *Move) ToString() string {
	return ""
}

func MovePairFromString(raw string, numTurn int) (MovePair, error) {
	parts := strings.Split(raw, " ")
	mp := MovePair{
		Source: raw,
		Num:    numTurn,
		Moves:  [2]Move{},
	}

	for i, part := range parts {
		mp.Moves[i] = MoveFromString(part)
	}

	if len(mp.Moves) < 1 {
		return mp, errors.New(fmt.Sprintf("illegal algebraic short move encountered, unable to parse: %s", raw))
	}

	return mp, nil
}

func MoveFromString(raw string) Move {
	piece := PiecePawn
	executes := false
	promotes := false
	cstl_ks := false
	cstl_qs := false
	target := []int32{}
	unique := []rune{}
	score := false

	// Either a castle move or a final score.
	if strings.Contains(raw, "-") {
		if strings.ToLower(raw) == "o-o" {
			cstl_ks = true
		} else if strings.ToLower(raw) == "o-o-o" {
			cstl_qs = true
		} else {
			score = true
			unique = []rune(raw)
		}
		// Normal move string, we will parse it rune by rune
	} else {
		for _, char := range []rune(raw) {
			mc := MoveCharacter(char)

			if mc.IsPiece() {
				piece = Piece(mc)
			} else if mc == MoveCharacterPromotes {
				promotes = true
			} else if mc == MoveCharacterExecutes {
				executes = true
			} else if mc.IsPositional() || mc.IsPositionNumeral() {
				target = append(target, char)
			}
		}

		if len(target) > 2 {
			unique = target[:len(target)-2]
			target = target[len(target)-2:]
		}
	}

	return Move{
		Piece:               piece,
		Target:              target,
		Unique:              unique,
		IsTaking:            executes,
		IsPromoting:         promotes,
		IsCastlingKingSide:  cstl_ks,
		IsCastlingQueenSide: cstl_qs,
		IsScoreCounter:      score,
	}
}

type MoveCharacter rune

const (
	MoveCharacterExecutes MoveCharacter = 'x'
	MoveCharacterPromotes               = '+'
)

func (mc MoveCharacter) IsPositional() bool {
	contains := false
	checkList := []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '1', '2', '3', '4', '5', '6', '7', '8'}
	for i := range checkList {
		if checkList[i] == rune(mc) {
			contains = true
			break
		}
	}

	return contains
}
func (mc MoveCharacter) IsPositionNumeral() bool {
	contains := false
	checkList := []rune{'1', '2', '3', '4', '5', '6', '7', '8'}
	for i := range checkList {
		if checkList[i] == rune(mc) {
			contains = true
			break
		}
	}

	return contains
}
func (mc MoveCharacter) IsPiece() bool {
	contains := false
	checkList := []Piece{PieceRook, PieceKnight, PieceBishop, PieceQueen, PieceKing}
	for i := range checkList {
		if rune(checkList[i]) == rune(mc) {
			contains = true
			break
		}
	}

	return contains
}

// ToLineRune takes an X coordinate between 0-7 and returns the
// corresponding chess grid rune between 'a'-'h'.
func ToLineRune(in int8) (rune, error) {
	if in > 7 {
		return 0, errors.New("input x coordinate exceeds limit")
	}

	return rune('a' + in), nil
}
func FromLineRune(in rune) (int32, error) {
	parse, err := strconv.Atoi(string(in - 'a'))
	if err != nil {
		return -1, err
	}

	return int32(parse), nil
}

// ToColumnRune takes an Y coordinate between 0-7 and returns the
// corresponding chess grid rune between '8'-'1'.
func ToColumnRune(in int8) (rune, error) {
	if in > 7 {
		return 0, errors.New("input y coordinate exceeds limit")
	}

	return rune('8' - in), nil
}
func FromColumnRune(in rune) (int8, error) {
	parse, err := strconv.Atoi(string(in))
	if err != nil {
		return -1, err
	}

	return int8(8 - parse), nil
}

type MovePairList []*MovePair

func (mpl MovePairList) Len() int           { return len(mpl) }
func (mpl MovePairList) Less(i, j int) bool { return mpl[i].Num < mpl[j].Num }
func (mpl MovePairList) Swap(i, j int)      { mpl[i], mpl[j] = mpl[j], mpl[i] }
