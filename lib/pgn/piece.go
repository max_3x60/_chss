package pgn

import (
	"chss/lib/fen"
	"errors"
	"fmt"
	"strings"
)

type Piece rune

func (p Piece) ToFEN(ply fen.Player) fen.Piece {
	if ply == fen.PlayerBlack {
		return fen.Piece([]rune(strings.ToLower(string(p)))[0])
	}

	return fen.Piece(p)
}

// ToAlgebraic returns piece in algebraic notation or error if conversion is
// not possible. Note that this lib uses Pawn as placeholder and will return
// an algebraic pawn rune, even though PGN has no character for pawns.
func FromFENPiece(p fen.Piece) (Piece, error) {
	switch p {
	case fen.WhitePawn,
		fen.BlackPawn:
		{
			return PiecePawn, nil
		}
	case fen.WhiteRook,
		fen.BlackRook:
		{
			return PieceRook, nil
		}
	case fen.WhiteKnight,
		fen.BlackKnight:
		{
			return PieceKnight, nil
		}
	case fen.WhiteBishop,
		fen.BlackBishop:
		{
			return PieceBishop, nil
		}
	case fen.WhiteQueen,
		fen.BlackQueen:
		{
			return PieceQueen, nil
		}
	case fen.WhiteKing,
		fen.BlackKing:
		{
			return PieceKing, nil
		}
	}

	return PiecePawn, errors.New(fmt.Sprintf("the supplied piece %q has no algebraic expression", p))
}

const (
	PieceRook   Piece = 'R'
	PieceKnight Piece = 'N'
	PieceBishop Piece = 'B'
	PieceQueen  Piece = 'Q'
	PieceKing   Piece = 'K'
	// PiecePawn doesn't exist in algebraic shorthand, but we need it as a placeholder.
	PiecePawn Piece = 'P'
)
