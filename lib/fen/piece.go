package fen

type Piece byte
type PieceList []Piece

const (
	NilPiece    Piece = '_'
	BlackPawn   Piece = 'p'
	BlackRook   Piece = 'r'
	BlackKnight Piece = 'n'
	BlackBishop Piece = 'b'
	BlackQueen  Piece = 'q'
	BlackKing   Piece = 'k'
	WhitePawn   Piece = 'P'
	WhiteRook   Piece = 'R'
	WhiteKnight Piece = 'N'
	WhiteBishop Piece = 'B'
	WhiteQueen  Piece = 'Q'
	WhiteKing   Piece = 'K'
	NewLine     Piece = '/'
)

func (p Piece) IsWhite() bool {
	switch p {
	case BlackPawn,
		BlackRook,
		BlackKing,
		BlackKnight,
		BlackQueen,
		BlackBishop,
		NilPiece,
		NewLine:
		{
			return false
		}

	case WhitePawn,
		WhiteRook,
		WhiteKing,
		WhiteKnight,
		WhiteQueen,
		WhiteBishop:
		{
			return true
		}
	}

	return false
}
func (p Piece) IsBlack() bool {
	switch p {
	case BlackPawn,
		BlackRook,
		BlackKing,
		BlackKnight,
		BlackQueen,
		BlackBishop:
		{
			return true
		}

	case WhitePawn,
		WhiteRook,
		WhiteKing,
		WhiteKnight,
		WhiteQueen,
		WhiteBishop,
		NilPiece,
		NewLine:
		{
			return false
		}
	}

	return false
}

func (p Piece) IsKing() bool {
	return p == BlackKing || p == WhiteKing
}
func (p Piece) IsRook() bool {
	return p == BlackRook || p == WhiteRook
}
func (p Piece) IsPawn() bool {
	return p == BlackPawn || p == WhitePawn
}

func (p Piece) Player() Player {
	if p.IsWhite() {
		return PlayerWhite
	}

	return PlayerBlack
}

// ToDo: Implement 'pnrbqk' sorting
func (pl PieceList) Len() int { return len(pl) }
func (pl PieceList) Less(i, j int) bool {
	if pl[i].IsPawn() && !pl[j].IsPawn() {
		return true
	} else if !pl[i].IsPawn() && pl[j].IsPawn() {
		return false
	}

	return pl[i] < pl[j]
}
func (pl PieceList) Swap(i, j int) { pl[i], pl[j] = pl[j], pl[i] }
