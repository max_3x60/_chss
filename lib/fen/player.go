package fen

type Player int

const (
	PlayerWhite Player = 'w'
	PlayerBlack        = 'b'
)

func (p Player) Opposite() Player {
	if p == PlayerBlack {
		return PlayerWhite
	}

	return PlayerBlack
}

func (p Player) ToString() string {
	if p == PlayerWhite {
		return "White"
	}

	return "Black"
}
