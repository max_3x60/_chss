package fen

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

var REGEXP_IS_FEN_COMPLIANT = "\\s*^(((?:[rnbqkpRNBQKP1-8]+\\/){7})[rnbqkpRNBQKP1-8]+)\\s([b|w])\\s([K|Q|k|q]{1,4})\\s(-|[a-h][1-8])\\s(\\d+\\s\\d+)$"

type FEN struct {
	source               string
	StartPiecePlacements []Piece
	SideToMove           Player
	Castling             []rune
	EnPassant            bool
	HalveMoveClock       int
	FullMoveNumber       int
}

func FromString(source string) (FEN, error) {
	if !Complies(source) && Complies(FENStringDefault) {
		return FromString(FENStringDefault)
	} else if !Complies(FENStringDefault) {
		return FEN{}, errors.New("lib can't parse input or default FEN string. Something is wrong")
	}

	finds := strings.Split(source, " ")

	enPassant := true
	hMove, fMove := 0, 0
	if string(finds[3]) == "-" {
		enPassant = false
	}
	if unicode.IsDigit([]rune(finds[4])[0]) {
		hMove, _ = strconv.Atoi(finds[4])
	}
	if unicode.IsDigit([]rune(finds[5])[0]) {
		fMove, _ = strconv.Atoi(finds[5])
	}

	return FEN{
		source:               source,
		StartPiecePlacements: []Piece(finds[0]),
		SideToMove:           Player([]rune(finds[1])[0]),
		Castling:             []rune(finds[2]),
		EnPassant:            enPassant,
		HalveMoveClock:       hMove,
		FullMoveNumber:       fMove,
	}, nil
}

func Complies(test string) bool {
	chk := regexp.MustCompile(REGEXP_IS_FEN_COMPLIANT)

	recover()
	return chk.Match([]byte(test))
}
