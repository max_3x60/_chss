module chss

go 1.18

require (
	github.com/faiface/beep v1.1.0
	github.com/hajimehoshi/ebiten/v2 v2.2.4
	github.com/jinzhu/copier v0.3.5
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210727001814-0db043d8d5be // indirect
	github.com/hajimehoshi/oto v0.7.1 // indirect
	github.com/jezek/xgb v0.0.0-20210312150743-0e0f116e1240 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/mobile v0.0.0-20210902104108-5d9a33257ab5 // indirect
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)
