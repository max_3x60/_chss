package app

import (
	"chss/app/core"
	"chss/app/ui"
	util "chss/app/util"
	"chss/lib/fen"
	"chss/lib/pgn"
	"embed"
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text"
	"image"
	"image/color"
	"image/jpeg"
	"image/png"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"
	"unicode"
)

var (
	DEBUG_AUTOPLAY             = false
	COMPUTER_OPPONENT          = false
	PLAYBACK_RECORDING         = false
	RECORDING_TURN_SPEED int64 = 300

	P_WORTH_KING   = 9000
	P_WORTH_QUEEN  = 900
	P_WORTH_BISHOP = 600
	P_WORTH_ROOK   = 500
	P_WORTH_KNIGHT = 300
	P_WORTH_PAWN   = 75
)

type PromotionCandidate struct {
	Piece fen.Piece
	Move  [5]int8
}

type Game struct {
	Width, Height int
	Audio         *util.AudioManager
	Buttons       []*ui.Button
	Boxes         []*ui.Box

	SelectedPiece core.Piece
	Match         core.Match
	IsOver        bool

	IsPromotionContext bool
	PromotionCandidate PromotionCandidate

	BackgroundImage *ebiten.Image

	BoardImage              *ebiten.Image
	BoardWidth, BoardHeight int
	TileDim                 int

	assetDir        embed.FS
	pieceImageCache map[fen.Piece]*ebiten.Image
	pieceSkin       string
}

func NewGame(width, height int, assetDir embed.FS) *Game {
	m, err := core.NewMatch(fen.FENStringDefault, []byte{})
	if err != nil {
		log.Fatal("can't create default match, something is very wrong")
	}

	bw, bh, bd := width-20*(width/100), height-10*(height/100), 0
	if bw > bh {
		bd = bh / 9
	} else {
		bd = bw / 9
	}

	bgRaw, err := assetDir.Open("assets/bg_abstract_cubes.jpg")
	bg, err := jpeg.Decode(bgRaw)
	if err != nil {
		log.Fatal(err)
	}

	game := &Game{
		Audio: util.NewAudioManager(assetDir),

		Width:  width,
		Height: height,

		BoardImage:  drawNewBoard(bw, bh, bd, util.NewDefaultBoardColors()),
		BoardWidth:  bw,
		BoardHeight: bh,
		TileDim:     bd,

		BackgroundImage: ebiten.NewImageFromImage(bg),

		SelectedPiece: core.Piece{byte(fen.NilPiece), 0, 0},
		Match:         m,

		IsPromotionContext: false,
		PromotionCandidate: PromotionCandidate{
			Piece: fen.NilPiece,
		},

		assetDir:        assetDir,
		pieceSkin:       "cartoon",
		pieceImageCache: map[fen.Piece]*ebiten.Image{},
	}

	offX, offY := game.BoardOffset()

	game.Buttons = []*ui.Button{
		{X: (width - bw) - 3*bd, Y: offY + game.TileDim*3, Width: bd * 2, Height: bd / 2, Label: "Start Autoplay", Color: util.ColorTurquiose, Font: GameFontNormal,
			OnClick: func(b *ui.Button) {
				DEBUG_AUTOPLAY = !DEBUG_AUTOPLAY
				COMPUTER_OPPONENT = false

				if DEBUG_AUTOPLAY {
					b.Color = util.ColorAlizarin
					b.Label = "Stop Autoplay"
				} else {
					b.Color = util.ColorTurquiose
					b.Label = "Start Autoplay"
				}
			}},
		{X: (width - bw) - 3*bd, Y: offY + game.TileDim*3 + bd, Width: bd * 2, Height: bd / 2, Label: "Reset Match", Color: util.ColorSunflower, Font: GameFontNormal, OnClick: func(b *ui.Button) {
			game.Reset()
			COMPUTER_OPPONENT = false
		}},
		{X: (width - bw) - 3*bd, Y: offY + game.TileDim*3 + 2*bd, Width: bd * 2, Height: bd / 2, Label: "VS Computer", Color: util.ColorPeterriver, Font: GameFontNormal, OnClick: func(b *ui.Button) {
			game.Reset()
			COMPUTER_OPPONENT = true
		}},
		{X: (width - bw) - 3*bd, Y: offY + game.TileDim*3 + 3*bd, Width: bd * 2, Height: bd / 2, Label: "Play Recording", Color: util.ColorSunflower, Font: GameFontNormal, OnClick: func(b *ui.Button) {
			COMPUTER_OPPONENT = false
			DEBUG_AUTOPLAY = false

			//f, err := game.assetDir.ReadFile("assets/matches/kasparov_v_topalov_1999.pgn")
			f, err := game.assetDir.ReadFile("assets/matches/kasparov_v_deepblue_19970511.pgn")
			//f, err := game.assetDir.ReadFile("assets/matches/stockfish_v_stockfish.pgn")
			if err != nil {
				log.Fatal(err)
			}

			match, err := core.NewMatch(fen.FENStringDefault, f)
			if err != nil {
				log.Fatal(err)
			}

			game.Match = match

			PLAYBACK_RECORDING = true
		}},
		{X: (width - bw) - 3*bd, Y: offY + game.TileDim*3 + 4*bd, Width: bd * 2, Height: bd / 2, Label: "Pause Recording", Color: util.ColorAlizarin, Font: GameFontNormal, OnClick: func(b *ui.Button) {
			PLAYBACK_RECORDING = false
		}},
	}

	game.Boxes = []*ui.Box{
		{
			Width: game.TileDim * 5, Height: game.TileDim * 4,
			X: offX + game.TileDim*9 + 20, Y: offY,
			BorderWidth: 3, BorderColor: util.ColorCharcoal,
			BackgroundColor: util.ColorDarkGrey, BackgroundOpacity: .5725,
		},
		{
			Width: game.TileDim * 5, Height: game.TileDim * 2,
			X: offX + game.TileDim*9 + 20, Y: offY + game.TileDim*6,
			BorderWidth: 3, BorderColor: util.ColorCharcoal,
			BackgroundColor: util.ColorDarkGrey, BackgroundOpacity: .5725,
		},

		{
			Width: game.TileDim * 3, Height: game.TileDim * 2,
			X: 20, Y: offY,
			BorderWidth: 3, BorderColor: util.ColorCharcoal,
			BackgroundColor: util.ColorDarkGrey, BackgroundOpacity: .5725,
		},
		{
			Width: game.TileDim * 3, Height: game.TileDim*6 - game.TileDim/2,
			X: 20, Y: offY + game.TileDim*2 + game.TileDim/2,
			BorderWidth: 3, BorderColor: util.ColorCharcoal,
			BackgroundColor: util.ColorDarkGrey, BackgroundOpacity: .5725,
		},
	}

	return game
}

var lastRecordingStep = time.Now()

func (g *Game) Update() error {
	g.Bindings()

	// Current side has only one king.
	if (g.Match.SideToMove == fen.PlayerWhite && g.Match.WhitePiecesCount() < 2) ||
		(g.Match.SideToMove == fen.PlayerBlack && g.Match.BlackPiecesCount() < 2) {
		g.IsOver = true
		return nil
	}

	//// AI Random Autoplay
	if DEBUG_AUTOPLAY && time.Now().UnixMilli()-lastRecordingStep.UnixMilli() > RECORDING_TURN_SPEED && !g.IsOver {
		mTree := core.NewTree(g.Match.Board, g.Match.SideToMove, 1)

		if len(mTree.Root.Children) < 1 {
			g.IsOver = true
			return nil
		}

		m := SelectMoveFromNodes(mTree.Root)

		if (g.Match.SideToMove != fen.PlayerWhite && g.Match.PieceAt(m[0], m[1]).IsWhite()) ||
			(g.Match.SideToMove != fen.PlayerBlack && g.Match.PieceAt(m[0], m[1]).IsBlack()) {
			return nil
		}

		g.Match.Move(m[0], m[1], [3]int8{m[2], m[3], m[4]})
		g.Audio.Play("piece_move")

		lastRecordingStep = time.Now()

		//// VS Random Opponent
	} else if COMPUTER_OPPONENT && g.Match.SideToMove == fen.PlayerBlack && time.Now().UnixMilli()-lastRecordingStep.UnixMilli() > RECORDING_TURN_SPEED && !g.IsOver {
		mTree := core.NewTree(g.Match.Board, g.Match.SideToMove, 1)

		if len(mTree.Root.Children) < 1 {
			g.IsOver = true
			return nil
		}

		m := SelectMoveFromNodes(mTree.Root)

		if (g.Match.SideToMove != fen.PlayerWhite && g.Match.PieceAt(m[0], m[1]).IsWhite()) ||
			(g.Match.SideToMove != fen.PlayerBlack && g.Match.PieceAt(m[0], m[1]).IsBlack()) {
			return nil
		}

		g.Match.Move(m[0], m[1], [3]int8{m[2], m[3], m[4]})
		g.Audio.Play("piece_move")

		lastRecordingStep = time.Now()

		//// Recording Playback
	} else if PLAYBACK_RECORDING && !g.IsOver && time.Now().UnixMilli()-lastRecordingStep.UnixMilli() > RECORDING_TURN_SPEED {
		lastRecordingStep = time.Now()

		if g.Match.FullMoveNumber < len(g.Match.Moves) {
			if !g.Match.Next() {
				g.IsOver = true
			} else {
				g.Audio.Play("piece_move")
			}
		}
	}

	// ToDo: Add win check here

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	//// DRAW: Background
	imgX, imgY := g.BackgroundImage.Size()
	op_bg := &ebiten.DrawImageOptions{}
	op_bg.GeoM.Scale(1/float64(imgX/g.Width), 1/float64(imgY/g.Height))
	op_bg.Filter = ebiten.FilterLinear
	screen.DrawImage(g.BackgroundImage, op_bg)

	//// DRAW: Board
	offX, offY := g.BoardOffset()
	op_board := &ebiten.DrawImageOptions{}
	op_board.GeoM.Translate(float64(offX), float64(offY))

	screen.DrawImage(g.BoardImage, op_board)
	if g.SelectedPiece.Kind() != fen.NilPiece {
		g.DrawIndicator(screen, op_board)
	}

	//// DRAW: Match pieces
	for _, p := range g.Match.Pieces() {
		if !p.Equals(g.SelectedPiece) {
			g.DrawPiece(p, false, screen, op_board)
		}
	}
	if !g.IsPromotionContext {
		g.DrawPiece(g.SelectedPiece, true, screen, op_board)
	}

	//// DRAW: UI
	for i := range g.Boxes {
		g.Boxes[i].Draw(screen)
	}
	for i := range g.Buttons {
		g.Buttons[i].Draw(screen)
	}
	g.DrawTextUI(screen)

	// Fallen Pieces
	op_fp_white := &ebiten.DrawImageOptions{}
	op_fp_white.GeoM.Translate(float64(g.BoardWidth), float64(g.Height/2))
	g.DrawFallenPieces(screen, g.Match.FallenWhitePieces(), 0)
	op_fp_black := &ebiten.DrawImageOptions{}
	op_fp_black.GeoM.Translate(float64(g.BoardWidth), float64(g.Height/2+g.Height/4))
	g.DrawFallenPieces(screen, g.Match.FallenBlackPieces(), g.TileDim)

	// Promotion selection window
	if g.IsPromotionContext {
		g.DrawPromotionSelection(screen)
	}
}

func (g *Game) DrawPiece(p core.Piece, isSelected bool, screen *ebiten.Image, op *ebiten.DrawImageOptions) {
	if p.Kind() == fen.NilPiece || p.Kind() == 0 {
		return
	}

	imgX, imgY := g.ImageFor(p.Kind()).Size()

	opCust := &ebiten.DrawImageOptions{}
	opCust.Filter = ebiten.FilterLinear
	opCust.GeoM.Scale(1/(float64(imgX)/float64(g.TileDim)), 1/(float64(imgY)/float64(g.TileDim)))

	if isSelected {
		mX, mY := ebiten.CursorPosition()
		opCust.GeoM.Translate(float64(mX-30), float64(mY-30))

		screen.DrawImage(g.ImageFor(p.Kind()), opCust)
	} else {
		opCust.GeoM.Translate(float64(int(p.X()+1)*g.TileDim), float64(int(p.Y())*g.TileDim))

		opCust.GeoM.Concat(op.GeoM)
		screen.DrawImage(g.ImageFor(p.Kind()), opCust)
	}
}
func (g *Game) DrawIndicator(screen *ebiten.Image, op *ebiten.DrawImageOptions) {
	indicator := ebiten.NewImageFromImage(image.Rect(0, 0, g.BoardWidth, g.BoardHeight))
	indicator.Fill(color.Transparent)

	tree := core.NewTree(g.Match.Board, g.Match.SideToMove, 1)
	colors := util.NewDefaultBoardColors()
	for _, c := range tree.Root.Children {
		// ToDo: draw castle and promotion move indicators

		if uint8(c.Move[0]) == g.SelectedPiece.X() && uint8(c.Move[1]) == g.SelectedPiece.Y() {
			x, y := c.Move[2], c.Move[3]

			c := colors.SelectedThreat
			if pTarget := g.Match.PieceAt(x, y); pTarget != fen.NilPiece {
				c = colors.SelectedTake
			}

			indicator.SubImage(image.Rect(
				int(x+1)*g.TileDim, int(y)*g.TileDim,
				int(x+2)*g.TileDim, int(y+1)*g.TileDim,
			)).(*ebiten.Image).Fill(c)
		}
	}
	indicator.SubImage(image.Rect(
		int(g.SelectedPiece.X()+1)*g.TileDim, int(g.SelectedPiece.Y())*g.TileDim,
		int(g.SelectedPiece.X()+2)*g.TileDim, int(g.SelectedPiece.Y()+1)*g.TileDim,
	)).(*ebiten.Image).Fill(colors.SelectedOrigin)

	screen.DrawImage(indicator, op)
}
func (g *Game) DrawFallenPieces(screen *ebiten.Image, block []fen.Piece, offY int) {
	sort.Sort(fen.PieceList(block))

	borderTopBottom, borderLeftRight := 10, 10
	tileSize := g.TileDim

	offX, _ := g.BoardOffset()
	width, height := offX-20, g.Height/4-20

	if width > height {
		tileSize = (height - borderTopBottom*2) / 4
	} else {
		tileSize = (width - borderLeftRight*2) / 8
	}

	for i, p := range block {
		op_img := &ebiten.DrawImageOptions{}
		op_img.Filter = ebiten.FilterLinear
		op_img.CompositeMode = ebiten.CompositeModeSourceOver
		op_img.GeoM.Translate(float64(10+offX+g.TileDim*9+(i%9)*tileSize), float64(offY+g.TileDim*7+(i/9)*tileSize))

		screen.DrawImage(g.ImageFor(p), op_img)
	}
}
func (g *Game) DrawTextUI(screen *ebiten.Image) {
	mTree := core.NewTree(g.Match.Board, g.Match.SideToMove, 1)
	oppMoves := 0
	isCheck := false
	for i := range mTree.Root.Children {
		oppMoves += len(mTree.Root.Children[i].Children)
	}
	kX, kY := uint8(0), uint8(0)
	for _, p := range g.Match.Pieces() { // check if enemy threatens our king
		if p.IsAvailableInTurn(g.Match.SideToMove) && (p.Kind() == fen.BlackKing || p.Kind() == fen.WhiteKing) {
			kX, kY = p.X(), p.Y()
		}
	}
	for _, p := range g.Match.Pieces() { // check if enemy threatens our king
		if p.IsAvailableInTurn(g.Match.SideToMove) {
			continue
		}

		bb, _ := core.NewBitBoard(&p, g.Match.Board)
		if bb.At(kX, kY) {
			isCheck = true
			break
		}
	}

	var infoText string
	infoText += fmt.Sprintf("Which Turn:         %s\n", g.Match.SideToMove.ToString())
	infoText += fmt.Sprintf("Possible Moves:     %d\n", len(mTree.Root.Children))
	infoText += fmt.Sprintf("Possible Responses: %d\n", oppMoves)
	if len(mTree.Root.Children) < 1 {
		infoText += "\n\n--- You are mate! ---\n"
	} else if isCheck {
		infoText += "\n\n--- You are in check! ---\n"
	}

	g.Boxes[2].SetText(infoText, GameFontNormal)

	if g.IsOver {
		if len(g.Match.Pieces()) > 2 {
			text.Draw(screen, fmt.Sprintf("Player %s Wins!", g.Match.SideToMove.Opposite().ToString()), GameFontGiant, g.Width-(g.BoardWidth/2)-500, g.Height-(g.BoardHeight/2)-50, util.ColorTurquiose)
		} else {
			text.Draw(screen, "\n\n--- It's a Draw! ---", GameFontGiant, g.Width-(g.BoardWidth/2)-500, g.Height-(g.BoardHeight/2)-50, util.ColorTurquiose)
		}
	}

	// Reverse sort move list
	var moves = pgn.MovePairList(g.Match.Moves)
	//sort.Sort(sort.Reverse(moves))

	// Print move list entry by entry
	var movesStr = ""
	for i := range moves {
		var mStr = fmt.Sprintf("%d. ", moves[i].Num)

		for _, m := range moves[i].Moves {
			if m.IsScoreCounter {
				mStr += string(m.Unique) + " | "
			} else if m.IsCastlingKingSide {
				mStr += "O-O | "
			} else if m.IsCastlingQueenSide {
				mStr += "O-O-O | "
			} else {
				if m.IsPromoting {
					mStr += fmt.Sprintf("%s %s %s%s | ", rune(m.Piece), string(m.Unique), string(m.Target[0]), string(m.Target[1]))
				} else if m.IsTaking {
					mStr += fmt.Sprintf("%s %s x %s%s | ", rune(m.Piece), string(m.Unique), string(m.Target))
				} else {
					if len(m.Target) > 1 {
						if unicode.ToUpper(rune(m.Piece)) == 'P' && len(m.Unique) < 1 {
							mStr += fmt.Sprintf("%s%s | ", string(m.Target[0]), string(m.Target[1]))
						} else {
							mStr += fmt.Sprintf("%s %s %s%s | ", rune(m.Piece), string(m.Unique), string(m.Target[0]), string(m.Target[1]))
						}
					}
				}
			}
		}

		movesStr += mStr + "\n"
	}

	g.Boxes[0].SetText(movesStr, GameFontSmall)
}
func (g *Game) DrawPromotionSelection(screen *ebiten.Image) {
	x, y := g.PromotionContextOffset()

	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(float64(x), float64(y))

	// Draw container
	screen.SubImage(image.Rect(x, y, x+g.TileDim*4+20, y+g.TileDim+20)).(*ebiten.Image).Fill(util.ColorDarkGrey)
	screen.SubImage(image.Rect(x+10, y+10, x+g.TileDim*4+10, y+g.TileDim+10)).(*ebiten.Image).Fill(util.ColorLightGrey)

	// Draw pieces
	for i, piece := range g.PromotionChoices() {
		x0, y0 := x+10+i*g.TileDim, y+10

		op := &ebiten.DrawImageOptions{}
		op.GeoM.Scale(float64(g.TileDim)/60, float64(g.TileDim)/60)
		op.GeoM.Translate(float64(x0), float64(y0))
		op.Filter = ebiten.FilterLinear

		if g.PromotionCandidate.Piece == piece {
			screen.SubImage(image.Rect(x0, y0, x0+g.TileDim, y0+g.TileDim)).(*ebiten.Image).Fill(util.NewDefaultBoardColors().SelectedTake)
		}

		screen.DrawImage(g.ImageFor(piece), op)
	}
}

func (g *Game) ImageFor(piece fen.Piece) *ebiten.Image {
	if res, chk := g.pieceImageCache[piece]; chk {
		return res
	}

	f, err := g.assetDir.Open("assets/pieces.png")
	img, err := png.Decode(f)
	if err != nil {
		log.Fatal(err)
	}

	sprite := ebiten.NewImageFromImage(img)
	order := []fen.Piece("qkrnbpQKRNBP")
	g.pieceImageCache = map[fen.Piece]*ebiten.Image{}
	for i, _ := range order {
		x0 := (i % 6) * 60
		y0 := (i / 6) * 60
		g.pieceImageCache[order[i]] = sprite.SubImage(image.Rect(x0, y0, x0+60, y0+60)).(*ebiten.Image)
	}

	return g.pieceImageCache[piece]
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return g.Width, g.Height
}

func (g *Game) Bindings() {
	mX, mY := ebiten.CursorPosition()

	if g.IsPromotionContext {
		pX, pY := g.PromotionContextOffset()
		pX += 10
		pY += 10

		if mX >= pX && mX < pX+4*g.TileDim && mY >= pY && mY < pY+g.TileDim {
			choices := g.PromotionChoices()

			g.PromotionCandidate.Piece = choices[(mX-pX)/g.TileDim]
		} else {
			g.PromotionCandidate.Piece = fen.NilPiece
		}

		if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonRight) {
			g.SelectedPiece.Reset()
			g.PromotionCandidate.Piece = fen.NilPiece
			g.IsPromotionContext = false
		} else if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) && g.PromotionCandidate.Piece != fen.NilPiece {
			cmd := core.MoveRuleDataPromoteQueen
			switch g.PromotionCandidate.Piece {
			case fen.BlackQueen,
				fen.WhiteQueen:
				{
					cmd = core.MoveRuleDataPromoteQueen
				}
			case fen.BlackBishop,
				fen.WhiteBishop:
				{
					cmd = core.MoveRuleDataPromoteBishop
				}
			case fen.BlackRook,
				fen.WhiteRook:
				{
					cmd = core.MoveRuleDataPromoteRook
				}
			case fen.BlackKnight,
				fen.WhiteKnight:
				{
					cmd = core.MoveRuleDataPromoteKnight
				}
			}

			m := g.PromotionCandidate.Move
			g.Match.Move(m[0], m[1], [3]int8{m[2], m[3], cmd})

			g.PromotionCandidate.Piece = fen.NilPiece
			g.IsPromotionContext = false
		}

		return
	}

	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) {
		fmt.Printf("M Left | %+v\n", g.SelectedPiece)
		lastRecordingStep = time.Now()

		offX, offY := g.BoardOffset()

		if mX > offX && mX < offX+g.BoardWidth && mY > offY && mY < offY+g.BoardHeight {
			bX, bY := int8((mX-offX)/g.TileDim), int8((mY-offY)/g.TileDim)
			bX -= 1

			if bX < 0 || bX > 7 || bY < 0 || bY > 7 {
				return
			}

			fmt.Printf("Hit %d, %d\n", bX, bY)

			p := g.Match.PieceAt(bX, bY)

			if p != fen.NilPiece && g.SelectedPiece.Kind() == fen.NilPiece {
				fmt.Println("Sel Piece")
				if g.Match.SideToMove == fen.PlayerWhite && p.IsBlack() || g.Match.SideToMove == fen.PlayerBlack && p.IsWhite() {
					return
				}

				g.SelectedPiece = core.Piece{byte(p), byte(bX), byte(bY)}
			} else if p != fen.NilPiece && g.SelectedPiece.Equals(core.Piece{byte(p), byte(bX), byte(bY)}) {
				fmt.Println("Place back")
				g.SelectedPiece.Reset()
			} else if g.SelectedPiece.Kind() != fen.NilPiece {
				// Calculate legal moves here
				tree := core.NewTree(g.Match.Board, g.Match.SideToMove, 1)

				// Loop through all possible moves and match with player request
				move, foundMove := [3]int8{}, false
				for _, c := range tree.Root.Children {
					// Are we trying to castle?
					if g.SelectedPiece.Kind().IsKing() && p.IsRook() {
						// Check if rooks are in place
						// ToDo: implement check 'was rook moved this match'
						if c.Move[4] == core.MoveRuleCastleKingside && bX == 7 {
							foundMove = true
							move = [3]int8{c.Move[2], c.Move[3], c.Move[4]}
							break
						} else if c.Move[4] == core.MoveRuleCastleQueenside && bX == 0 {
							foundMove = true
							move = [3]int8{c.Move[2], c.Move[3], c.Move[4]}
							break
						}
					}

					if c.Move == [5]int8{int8(g.SelectedPiece.X()), int8(g.SelectedPiece.Y()), bX, bY, c.Move[4]} && c.IsSane {
						if g.SelectedPiece.Kind().IsPawn() && (bY == 0 || bY == 7) {
							fmt.Println("Pawn and correct row", core.IsRulePromote(c.Move[4]), c.Move[4])

							foundMove = true
							move = [3]int8{c.Move[2], c.Move[3], core.MoveRulePromoteNoTakePiece} // ToDo: somehow promotion move rules don't make it through, fix it!
							break
						} else {
							foundMove = true
							move = [3]int8{c.Move[2], c.Move[3], c.Move[4]}
							break
						}
					}
				}

				// If player move is legal, apply to match state
				if foundMove {
					if core.IsRulePromote(move[2]) {
						g.PromotionCandidate = PromotionCandidate{
							Piece: fen.NilPiece,
							Move:  [5]int8{int8(g.SelectedPiece.X()), int8(g.SelectedPiece.Y()), move[0], move[1], move[2]},
						}
						g.IsPromotionContext = true
					} else {
						g.Match.Move(int8(g.SelectedPiece.X()), int8(g.SelectedPiece.Y()), move)
						g.SelectedPiece.Reset()
						g.Audio.Play("piece_move")
					}
				}
			}
		} else if b := g.GetButtonAt(mX, mY); b != nil {
			fmt.Println("Button Hit")
			b.OnClick(b)
			g.Audio.Play("button_onclick")
		}
	}
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonRight) {
		fmt.Println("M Right")
		g.SelectedPiece.Reset()
		g.Audio.Play("piece_move")
	}
}

func (g *Game) GetButtonAt(mX, mY int) *ui.Button {
	for _, b := range g.Buttons {
		// Bounding rect check
		if mX >= b.X && mX <= b.X+b.Width &&
			mY >= b.Y && mY <= b.Y+b.Height {

			return b
		}
	}

	return nil
}

func (g *Game) Reset() {
	m, err := core.NewMatch(fen.FENStringDefault, []byte{})
	if err != nil {
		log.Fatal("can't create default match, something is very wrong")
	}

	g.IsOver = false
	g.IsPromotionContext = false
	g.SelectedPiece.Reset()
	g.PromotionCandidate = PromotionCandidate{Piece: fen.NilPiece}

	g.Match = m
}

func (g *Game) BoardOffset() (int, int) {
	return g.Width - g.BoardWidth, (g.Height-g.BoardHeight)/2 + g.TileDim/2
}
func (g *Game) PromotionContextOffset() (int, int) {
	bX, _ := g.BoardOffset()

	return bX + g.TileDim*3 - 10, g.Height/2 - (g.TileDim*2-10)/2
}
func (g *Game) PromotionChoices() []fen.Piece {
	pieces := "qbrn"
	if g.Match.SideToMove == fen.PlayerWhite {
		pieces = strings.ToUpper(pieces)
	}

	return []fen.Piece(pieces)
}

func drawNewBoard(width, height, dimension int, colors *util.BoardColors) *ebiten.Image {
	cleanBoard := ebiten.NewImageFromImage(image.Rect(0, 0, 9*dimension, 9*dimension))
	cleanBoard.Fill(color.RGBA{0, 0, 0, 115})

	for x := 0; x < 9; x++ {
		for y := 0; y < 9; y++ {
			tile := cleanBoard.SubImage(image.Rect(
				x*dimension, y*dimension,
				(x+1)*dimension-2, (y+1)*dimension-2,
			)).(*ebiten.Image)
			tileShadow := cleanBoard.SubImage(image.Rect(
				x*dimension, y*dimension,
				(x+1)*dimension, (y+1)*dimension,
			)).(*ebiten.Image)

			if x < 1 || y > 7 {
				if y <= 7 {
					text.Draw(tile, strconv.Itoa(8-y), GameFontBig, dimension/2-10, y*dimension+dimension/2+10, colors.IndicatorText)
				} else {
					lst := []string{"", "A", "B", "C", "D", "E", "F", "G", "H"}
					text.Draw(tile, lst[x], GameFontBig, x*dimension+dimension/3, y*dimension+dimension/2+10, colors.IndicatorText)
				}
			} else {
				// DRAW BOARD
				if (x+y)%2 == 0 {
					tileShadow.Fill(colors.Odd.TileShadow)
					tile.Fill(colors.Odd.Tile)

				} else {
					tileShadow.Fill(colors.Even.TileShadow)
					tile.Fill(colors.Even.Tile)
				}
			}
		}
	}

	return cleanBoard
}
