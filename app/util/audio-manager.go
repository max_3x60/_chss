package util

import (
	"embed"
	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"log"
	"time"
)

type AudioCacheEntry struct {
	Stream beep.StreamSeekCloser
	Format beep.Format
}
type AudioManager struct {
	dataDir embed.FS
}

const AM_SAMPLE_RATE = 44100

func NewAudioManager(dir embed.FS) *AudioManager {
	am := AudioManager{dataDir: dir}

	sr := beep.SampleRate(AM_SAMPLE_RATE)
	err := speaker.Init(sr, sr.N(time.Second/25))
	if err != nil {
		log.Fatal(err)
	}

	return &am
}

func (am *AudioManager) Play(id string) {
	f, err := am.dataDir.Open("assets/sound/" + id + ".wav")
	if err != nil {
		log.Fatal(err)
	}

	streamer, _, err := wav.Decode(f)
	if err != nil {
		log.Fatal(err)
	}

	speaker.Play(streamer)

}
