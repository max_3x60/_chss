package util

import "image/color"

var ColorTurquiose = color.RGBA{26, 188, 156, 255}
var ColorAlizarin = color.RGBA{231, 76, 60, 255}
var ColorSunflower = color.RGBA{241, 196, 15, 255}
var ColorPeterriver = color.RGBA{52, 152, 219, 255}
var ColorDesert = color.RGBA{204, 174, 98, 255}
var ColorLightGrey = color.RGBA{209, 204, 192, 255}
var ColorDarkGrey = color.RGBA{170, 166, 157, 255}
var ColorCharcoal = color.RGBA{54, 69, 79, 255}
