package util

import "image/color"

type BoardColors struct {
	Odd, Even      *BoardTileColors
	SelectedTake   color.Color
	SelectedThreat color.Color
	SelectedOrigin color.Color
	IndicatorText  color.Color
}
type BoardTileColors struct {
	Tile       color.Color
	TileShadow color.Color
	Selected   color.Color
}

func NewDefaultBoardColors() *BoardColors {
	return &BoardColors{
		Odd: &BoardTileColors{
			Tile:       color.RGBA{134, 138, 63, 255}, //color.RGBA{R: 235, G: 233, B: 224, A: 255},
			TileShadow: color.RGBA{134, 138, 63, 255}, //color.RGBA{R: 243, G: 242, B: 236, A: 255},
			Selected:   color.RGBA{},
		},
		Even: &BoardTileColors{
			Tile:       color.RGBA{214, 219, 107, 255}, //color.RGBA{R: 246, G: 239, B: 233, A: 255},
			TileShadow: color.RGBA{229, 234, 122, 255}, // color.RGBA{R: 226, G: 219, B: 220, A: 255},
			Selected:   color.RGBA{},
		},
		IndicatorText:  color.White,
		SelectedTake:   color.RGBA{255, 255, 110, 200},
		SelectedThreat: color.RGBA{230, 50, 0, 125},
		SelectedOrigin: color.RGBA{230, 50, 0, 185},
	}
}
