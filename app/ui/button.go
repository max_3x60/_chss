package ui

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text"
	"golang.org/x/image/font"
	"image"
	"image/color"
)

type Button struct {
	X, Y          int
	Width, Height int
	Label         string
	Color         color.RGBA
	Font          font.Face

	OnClick func(*Button)

	_renderImage *ebiten.Image
}

func (b *Button) Draw(canvas *ebiten.Image) {
	if b._renderImage == nil {
		b.Update()
	}

	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(float64(b.X), float64(b.Y))
	op.CompositeMode = ebiten.CompositeModeSourceOver

	canvas.DrawImage(b._renderImage, op)
}

func (b *Button) Update() {
	b._renderImage = ebiten.NewImageFromImage(image.Rect(0, 0, b.Width, b.Height))
	b._renderImage.Fill(b.Color)

	text.Draw(b._renderImage, b.Label, b.Font, 20, b.Height/2+8, color.White) // ToDo: get font height use to center text.
}
