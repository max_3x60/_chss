package ui

import (
	"chss/app/util"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text"
	"golang.org/x/image/font"
	"image"
	"image/color"
	"strings"
)

type Box struct {
	Width, Height int
	X, Y          int

	BorderWidth int
	BorderColor color.RGBA

	BackgroundColor   color.RGBA
	BackgroundOpacity float64

	_renderImage  *ebiten.Image
	_contentImage *ebiten.Image
}

func (b *Box) Draw(screen *ebiten.Image) {
	if b._renderImage == nil {
		b.renderImage()
	}

	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(float64(b.X), float64(b.Y))
	op.CompositeMode = ebiten.CompositeModeSourceOver

	screen.DrawImage(b._renderImage, op)

	if b._contentImage != nil {
		ci_op := &ebiten.DrawImageOptions{}
		ci_op.CompositeMode = ebiten.CompositeModeSourceOver
		ci_op.GeoM.Translate(float64(b.X+b.BorderWidth), float64(b.Y+b.BorderWidth))
		screen.DrawImage(b._contentImage, ci_op)
	}
}

func (b *Box) SetText(content string, textFont font.Face) {
	if len(content) > 0 {
		b._contentImage = ebiten.NewImageFromImage(image.Rect(0, 0, b.Width-b.BorderWidth*2, b.Height-b.BorderWidth*2))
		b._contentImage.Fill(color.Transparent)

		lines := strings.Split(content, "\n")
		fontHeight := 18

		for i, line := range lines {
			text.Draw(b._contentImage, line, textFont, 5, (i+1)*fontHeight, color.White)
		}
	}
}

func (b *Box) renderImage() {
	tb_moves_inner := ebiten.NewImageFromImage(image.Rect(0, 0, b.Width-b.BorderWidth*2, b.Height-b.BorderWidth*2))
	tb_moves_inner.Fill(util.ColorLightGrey)
	tb_moves_inner_op := &ebiten.DrawImageOptions{}
	tb_moves_inner_op.CompositeMode = ebiten.CompositeModeSourceIn
	tb_moves_inner_op.ColorM.Scale(1, 1, 1, b.BackgroundOpacity)
	tb_moves_inner_op.GeoM.Translate(float64(b.BorderWidth), float64(b.BorderWidth))

	b._renderImage = ebiten.NewImageFromImage(image.Rect(0, 0, b.Width, b.Height))
	b._renderImage.Fill(b.BorderColor)

	b._renderImage.DrawImage(tb_moves_inner, tb_moves_inner_op)
}
