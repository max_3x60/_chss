package core

import (
	fen2 "chss/lib/fen"
	"fmt"
)

type Piece [3]byte

func (p Piece) Kind() fen2.Piece {
	return fen2.Piece(p[0])
}
func (p Piece) X() uint8 {
	return p[1]
}
func (p Piece) Y() uint8 {
	return p[2]
}

func (p Piece) Equals(cpr Piece) bool {
	return p[0] == cpr[0] && p[1] == cpr[1] && p[2] == cpr[2]
}

func (p *Piece) Reset() {
	p[0] = byte(fen2.NilPiece)
	p[1] = 0
	p[2] = 0
}

func (p *Piece) ToString() string {
	return fmt.Sprintf("[%q at (%d,%d)]", p.Kind(), p.X, p.Y)
}

func (p *Piece) IsAvailableInTurn(ply fen2.Player) bool {
	return (ply == fen2.PlayerWhite && p.Kind().IsWhite()) || (ply == fen2.PlayerBlack && p.Kind().IsBlack())
}
