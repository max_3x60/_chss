package core

import (
	"chss/lib/fen"
)

type BitBoard [64]bool

// BitBoardIndex list of valid move rules [3]int.
type BitBoardIndex [][3]int8

func NewBitBoard(p *Piece, board Board) (BitBoard, BitBoardIndex) {
	var bb BitBoard
	var idx BitBoardIndex
	rules := RuleSetForType(p.Kind())

	for _, rule := range rules {
		x, y := int8(p.X()), int8(p.Y())

		switch rule[2] {

		case MoveRuleDefault:
			{
				x += rule[0]
				y += rule[1]

				if x < 8 && y < 8 && x >= 0 && y >= 0 {
					bb[x+y*8] = true

					if targetP := board.PieceAt(x, y); targetP != fen.NilPiece {
						if rule[2] == 3 {
							if targetP.IsWhite() != p.Kind().IsWhite() {
								bb[x+y*8] = false
							} else {
								idx = append(idx, [3]int8{x, y, rule[2]})
							}
						} else {
							if targetP.IsWhite() == p.Kind().IsWhite() {
								bb[x+y*8] = false
							} else {
								idx = append(idx, [3]int8{x, y, rule[2]})
							}
						}
					} else {
						idx = append(idx, [3]int8{x, y, rule[2]})
					}
				}
			}
		case MoveRuleEnPassant:
			{
				x += rule[0]
				y += rule[1]

				if x < 7 && x >= 0 && y < 7 && y >= 0 {
					if targetP := board.PieceAt(x, y-rule[1]); targetP != fen.NilPiece {
						if targetP.IsBlack() != p.Kind().IsBlack() {
							bb[x+y*8] = true
							idx = append(idx, [3]int8{x, y, rule[2]})
						}
					}
				}
			}
		case MoveRuleAlongAxis:
			{
				for i := 0; i < 8; i++ {
					x += rule[0]
					y += rule[1]

					if x < 0 || x > 7 || y < 0 || y > 7 {
						break
					}

					bb[x+y*8] = true

					if targetP := board.PieceAt(x, y); targetP != fen.NilPiece {
						if targetP.IsBlack() == p.Kind().IsBlack() {
							bb[x+y*8] = false
						} else {
							idx = append(idx, [3]int8{x, y, rule[2]})
						}
						break
					} else {
						idx = append(idx, [3]int8{x, y, rule[2]})
					}
				}
			}
		case MoveRuleNeedTakePiece:
			{
				x += rule[0]
				y += rule[1]

				if x < 8 && x >= 0 && y < 8 && y >= 0 {
					if pTarget := board.PieceAt(x, y); pTarget != fen.NilPiece {
						if pTarget.IsBlack() != p.Kind().IsBlack() {
							bb[x+y*8] = true
							idx = append(idx, [3]int8{x, y, rule[2]})
						}
					}
				}
			}
		case MoveRuleNoTakePiece:
			{
				x += rule[0]
				y += rule[1]

				if x < 8 && x >= 0 && y < 8 && y >= 0 {
					if pTarget := board.PieceAt(x, y); pTarget == fen.NilPiece {
						bb[x+y*8] = true
						idx = append(idx, [3]int8{x, y, rule[2]})
					}
				}
			}
		case MoveRulePawnFirstMove:
			{
				x += rule[0]
				y += rule[1]

				if (p.Kind().IsWhite() && p.Y() == 6) || (p.Kind().IsBlack() && p.Y() == 1) {
					if x < 8 && y < 8 && x >= 0 && y >= 0 {
						if pTarget := board.PieceAt(x, y); pTarget == fen.NilPiece {
							bb[x+y*8] = true
							idx = append(idx, [3]int8{x, y, rule[2]})
						}
					}
				}
			}
		case MoveRuleCastleQueenside:
			{
				if p.Kind().IsBlack() && p.X() == 4 && p.Y() == 0 {
					if tRook := board.PieceAt(0, 0); tRook == fen.BlackRook {
						// ToDo: check if rook was moved this game

						// Make sure target area is clear
						if t1 := board.PieceAt(3, 0); t1 == fen.NilPiece {
							if t2 := board.PieceAt(2, 0); t2 == fen.NilPiece {
								bb[x+y*8] = true
								idx = append(idx, [3]int8{x, y, rule[2]})
							}
						}
					}
				}
				if p.Kind().IsWhite() && p.X() == 4 && p.Y() == 7 {
					if tRook := board.PieceAt(0, 7); tRook == fen.WhiteRook {
						// ToDo: check if rook was moved this game

						// Make sure target area is clear
						if t1 := board.PieceAt(3, 7); t1 == fen.NilPiece {
							if t2 := board.PieceAt(2, 7); t2 == fen.NilPiece {
								bb[x+y*8] = true
								idx = append(idx, [3]int8{x, y, rule[2]})
							}
						}
					}
				}
			}
		case MoveRuleCastleKingside:
			{
				if p.Kind().IsBlack() && p.X() == 4 && p.Y() == 0 {
					if tRook := board.PieceAt(7, 0); tRook == fen.BlackRook {
						// ToDo: check if rook was moved this game

						// Make sure target area is clear
						if t1 := board.PieceAt(6, 0); t1 == fen.NilPiece {
							if t2 := board.PieceAt(5, 0); t2 == fen.NilPiece {
								bb[x+y*8] = true
								idx = append(idx, [3]int8{x, y, rule[2]})
							}
						}
					}
				}
				if p.Kind().IsWhite() && p.X() == 4 && p.Y() == 7 {
					if tRook := board.PieceAt(7, 7); tRook == fen.WhiteRook {
						// ToDo: check if rook was moved this game

						// Make sure target area is clear
						if t1 := board.PieceAt(6, 7); t1 == fen.NilPiece {
							if t2 := board.PieceAt(5, 7); t2 == fen.NilPiece {
								bb[x+y*8] = true
								idx = append(idx, [3]int8{x, y, rule[2]})
							}
						}
					}
				}
			}
		case MoveRuleKing2Distance:
			{
				x += rule[0]
				y += rule[1]

				if x < 8 && y < 8 && x >= 0 && y >= 0 {
					// Check target area for enemy king within 1 tile.
					kingInSight := false
					for i := int8(-1); i < 2; i++ {
						for j := int8(-1); j < 2; j++ {
							if x+i > 7 || x+i < 0 || y+j > 7 || y+j < 0 {
								continue
							}

							chk := board.PieceAt(x+i, y+j)

							if p.Kind().IsWhite() && chk == fen.BlackKing ||
								p.Kind().IsBlack() && chk == fen.WhiteKing {

								kingInSight = true
							}
						}
					}

					// We are clear to move!
					if !kingInSight && ((p.Kind().IsWhite() && !board.PieceAt(x, y).IsWhite()) || (p.Kind().IsBlack() && !board.PieceAt(x, y).IsBlack())) {
						bb[x+y*8] = true
						idx = append(idx, [3]int8{x, y, rule[2]})
					}
				}
			}
		case MoveRulePromoteTakePiece:
			{
				// ToDo: implement
			}
		case MoveRulePromoteNoTakePiece:
			{
				// ToDo: implement
			}
		}
	}

	return bb, idx
}

func (bb BitBoard) At(x, y uint8) bool {
	if x > 7 || y > 7 {
		return false
	}

	return bb[x+y*8]
}

func (bb BitBoard) ToString() string {
	out := "----------------------------\n|"
	for i, b := range bb {
		if i%8 == 0 && i > 0 {
			out += "|\n"
		}

		if b {
			out += "1"
		} else {
			out += "0"
		}
	}
	out += "----------------------------\n"

	return out
}
