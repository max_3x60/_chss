package core

import (
	"chss/lib/fen"
)

type Tree struct {
	Root      *TreeNode
	WhichTurn fen.Player
}

type TreeNode struct {
	Parent   *TreeNode
	Children []*TreeNode

	// Move from_x,from_y, to_x,to_y, move_rule
	Move  [5]int8
	State Board
	// Worth as a [white,black] tuple, containing the delta in total piece worth for this move.
	Worth          [2]int16
	MaxWorthChilds [2]int

	IsSane bool
}

// IsLeaf Indicates if we have reached a dead end on our tree.
func (n *TreeNode) IsLeaf() bool { return len(n.Children) == 0 }

func NewTree(board Board, turn fen.Player, depth int) *Tree {
	tree := &Tree{
		Root: &TreeNode{
			State:  board,
			IsSane: true,
			Worth:  [2]int16{0, 0},
		},
		WhichTurn: turn,
	}
	tree.ComputeNode(turn, depth, tree.Root)

	return tree
}

// ComputeNode will calculate all children until it reaches the given recursion depth.
func (tree *Tree) ComputeNode(turn fen.Player, depth int, parent *TreeNode) {
	if depth < 0 {
		return
	}

	// Loop through all pieces on the board
	for _, p := range parent.State.Pieces() {
		// Discard enemy pieces
		if p.Kind().IsBlack() && turn == fen.PlayerWhite ||
			p.Kind().IsWhite() && turn == fen.PlayerBlack {
			continue
		}

		_, idx := NewBitBoard(&p, parent.State)

		// Loop trough every possible move
		for _, move := range idx {
			var board = parent.State

			if !board.TryMove(int8(p.X()), int8(p.Y()), move) {
				continue
			}

			worth_w_before := board.WhitePiecesWorth()
			worth_b_before := board.BlackPiecesWorth()
			board.Move(int8(p.X()), int8(p.Y()), move)
			worth_w_after := board.WhitePiecesWorth()
			worth_b_after := board.BlackPiecesWorth()

			// Can we take the enemy king?
			if (turn == fen.PlayerWhite && board.IsBlackKingDead()) ||
				(turn == fen.PlayerBlack && board.IsWhiteKingDead()) {

				// Mark previous enemy move as insane, or at least unthoughtful :D
				parent.IsSane = false
			} else {
				// Make a new tree node for the current move
				var n = TreeNode{
					Move:   [5]int8{int8(p.X()), int8(p.Y()), move[0], move[1], move[2]},
					State:  board,
					Worth:  [2]int16{worth_w_after - worth_w_before, worth_b_after - worth_b_before},
					Parent: parent,
					IsSane: true,
				}

				// Start computation of possible responses
				tree.ComputeNode(turn.Opposite(), depth-1, &n)

				// Check if enemy has taken our king as response, if not, add node to tree.
				if n.IsSane {
					parent.Children = append(parent.Children, &n)
				}
			}
		}

		// Pick out the best children
		var max_w = int16(-10000)
		var max_w_i = 0
		var max_b = int16(-10000)
		var max_b_i = 0
		for i, child := range parent.Children {
			if child.Worth[0] >= max_w {
				max_w = child.Worth[0]
				max_w_i = i
			}
			if child.Worth[1] >= max_b {
				max_b = child.Worth[1]
				max_b_i = i
			}
		}

		parent.MaxWorthChilds = [2]int{max_w_i, max_b_i}
	}
}
