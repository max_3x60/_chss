package core

import (
	"chss/lib/fen"
	pgn2 "chss/lib/pgn"
	"errors"
	"fmt"
	"log"
	"strconv"
	"unicode"
)

type Match struct {
	fen.FEN
	pgn2.PGN
	Board
}

func NewMatch(start string, rawData []byte) (Match, error) {
	match := Match{
		Board: NewBoard(),
	}

	parsed, err := fen.FromString(start)
	if err != nil {
		return Match{}, err
	}
	match.FEN = parsed

	if len(rawData) < 1 {
		match.PGN = pgn2.PGN{
			Moves:    []*pgn2.MovePair{},
			Comments: []*pgn2.Comment{},
		}
	} else {
		data, err := pgn2.From(parsed, rawData)
		if err != nil {
			return Match{}, err
		}

		match.PGN = data
	}

	ptr := 0
	for _, piece := range match.FEN.StartPiecePlacements {
		if piece == fen.NewLine {
			continue
		}
		if unicode.IsDigit(rune(piece)) {
			c, _ := strconv.Atoi(string(rune(piece)))
			ptr += c
			continue
		}

		match.Board.SetPiece(int8(ptr%8), int8(ptr/8), piece)

		ptr++
	}

	fmt.Println("Board: ", match.Board)

	return match, nil
}

func (m *Match) Move(x, y int8, move [3]int8) {
	piece, err := pgn2.FromFENPiece(m.Board.PieceAt(x, y))
	halfTurn := 0
	if m.SideToMove == fen.PlayerBlack {
		halfTurn++
	}
	targetP := m.Board.PieceAt(move[0], move[1])
	if err != nil {
		log.Fatal(fmt.Sprintf("Invalid move in Match.Move: %q@(%d, %d) -> %+v", m.Board.PieceAt(x, y), x, y, move))
	}

	if m.Board.Move(x, y, move) {
		if len(m.Moves) < m.FullMoveNumber {
			m.Moves = append(m.Moves, &pgn2.MovePair{
				Num:   m.FullMoveNumber,
				Moves: [2]pgn2.Move{},
			})
		}

		tX, err := pgn2.ToLineRune(move[0])
		if err != nil {
			log.Fatal(fmt.Sprintf("Invalid target in Match.Move: %q@(%d, %d) -> %+v", m.Board.PieceAt(x, y), x, y, move))
		}
		tY, err := pgn2.ToColumnRune(move[1])
		if err != nil {
			log.Fatal(fmt.Sprintf("Invalid target in Match.Move: %q@(%d, %d) -> %+v", m.Board.PieceAt(x, y), x, y, move))
		}

		if IsRulePromote(move[2]) {
			if move[2] == MoveRuleDataPromoteQueen {
				piece = pgn2.PieceQueen
			} else if move[2] == MoveRuleDataPromoteBishop {
				piece = pgn2.PieceBishop
			} else if move[2] == MoveRuleDataPromoteRook {
				piece = pgn2.PieceRook
			} else if move[2] == MoveRuleDataPromoteKnight {
				piece = pgn2.PieceKnight
			}
		}

		m.Moves[m.FullMoveNumber-1].Moves[m.HalveMoveClock%2] = pgn2.Move{
			Piece:               piece,
			Target:              []int32{tX, tY},
			Unique:              nil, // ToDo: implement check for 'unique identifier needed'
			IsTaking:            targetP != fen.NilPiece,
			IsPromoting:         IsRulePromote(move[2]),
			IsCastlingKingSide:  move[2] == MoveRuleCastleKingside,
			IsCastlingQueenSide: move[2] == MoveRuleCastleQueenside,
			IsScoreCounter:      false,
		}

		m.SideToMove = m.SideToMove.Opposite()
		m.HalveMoveClock++

		if m.HalveMoveClock%2 == 0 {
			m.FullMoveNumber++
		}
	}
}

func (m *Match) Next() bool {
	fmt.Println("Match.Next: ", m.HalveMoveClock)

	move, err := m.HalveMove(m.HalveMoveClock)
	if err != nil {
		return false
	}
	if move.IsScoreCounter {
		return false
	}

	fmt.Println(fmt.Sprint("Player ", string(m.SideToMove), " moves ", string(move.Piece), " to ", string(move.Target), " with '", string(move.Unique), "'"))

	// Now we need to transform an PGN move to something we can work with.
	// Let's get all possible moves for the current board first.
	tree := NewTree(m.Board, m.SideToMove, 1)
	if len(tree.Root.Children) < 1 {
		return false
	}

	// Loop through and filter for the one we want
	treeMove := [5]int8{}
	foundMove := false
	for _, c := range tree.Root.Children {
		// Convert back to a PGN grid.
		cX, _ := pgn2.ToLineRune(c.Move[0])
		cY, _ := pgn2.ToColumnRune(c.Move[1])
		ctX, _ := pgn2.ToLineRune(c.Move[2])
		ctY, _ := pgn2.ToColumnRune(c.Move[3])

		// Castling move one
		if move.IsCastlingKingSide {
			if c.Move[4] == MoveRuleCastleKingside {
				treeMove = c.Move
				foundMove = true
				break
			}
			continue

			// And castling move two
		} else if move.IsCastlingQueenSide {
			if c.Move[4] == MoveRuleCastleQueenside {
				treeMove = c.Move
				foundMove = true
				break
			}
			continue

			// pawn promotion
		} else if move.IsPromoting && IsRulePromote(c.Move[4]) {
			treeMove = c.Move
			foundMove = true

			switch move.Piece {
			case pgn2.PieceQueen:
				{
					treeMove[4] = MoveRuleDataPromoteQueen
				}
			case pgn2.PieceBishop:
				{
					treeMove[4] = MoveRuleDataPromoteBishop
				}
			case pgn2.PieceRook:
				{
					treeMove[4] = MoveRuleDataPromoteRook
				}
			case pgn2.PieceKnight:
				{
					treeMove[4] = MoveRuleDataPromoteKnight
				}
			}

			break

			// filter out a candidate
		} else if m.Board.PieceAt(c.Move[0], c.Move[1]) == move.Piece.ToFEN(m.SideToMove) {
			// Is the command ambiguous? If so, perform check on candidate
			if len(move.Unique) > 0 {
				unq := pgn2.MoveCharacter(move.Unique[0])

				if unq.IsPositional() && !unq.IsPositionNumeral() {
					// Filter out by row criteria
					if cX != move.Unique[0] {
						continue
					}

				} else if unq.IsPositional() && unq.IsPositionNumeral() {
					// Filter out by column criteria
					if cY != move.Unique[0] {
						continue
					}
				}
			}

			if move.Target[0] == ctX && move.Target[1] == ctY {
				// select candidate
				treeMove = c.Move
				foundMove = true
				break
			}
		}
	}

	// Apply selected move candidate if found
	if foundMove {
		if !m.Board.Move(treeMove[0], treeMove[1], [3]int8{treeMove[2], treeMove[3], treeMove[4]}) {
			return false
		}

		m.HalveMoveClock++
		if m.HalveMoveClock%2 == 0 {
			m.FullMoveNumber++
		}
		m.SideToMove = m.SideToMove.Opposite()

		fmt.Println("Move found, proceeding to -> ", m.HalveMoveClock, m.FullMoveNumber)

		return true
	}

	fmt.Println("No legal move found, game over.")

	return false
}

func (m *Match) Rewind(turnNum int, halfTurnNum int) error {
	if turnNum >= m.FullMoveNumber {
		if halfTurnNum > m.HalveMoveClock {
			return errors.New("Turn lies in the future, can't do that.")
		}
	}

	// ToDo: Skip through moves from beginning until desired point.

	return nil
}
