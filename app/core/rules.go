package core

import (
	"chss/lib/fen"
)

// MoveRuleSet consists of an x,y vector part and a
// third MoveRule, describing whether the vector should be:
// * 0 : Static
// * -4: Castle Queenside
// * -3: Castle Kingside
// * -2: En-passant
// * -1: Infinite
// * 1 : Take piece
// * 2 : Pawn first move
type MoveRuleSet [][3]int8

const (
	// MoveRuleCastleQueenside will have empty coordinates in MoveRuleSet.
	MoveRuleCastleQueenside int8 = iota
	// MoveRuleCastleKingside will have empty coordinates in MoveRuleSet.
	MoveRuleCastleKingside
	MoveRuleEnPassant
	MoveRuleAlongAxis
	MoveRuleDefault
	MoveRuleNeedTakePiece
	MoveRulePawnFirstMove
	MoveRuleKing2Distance
	MoveRuleNoTakePiece
	MoveRulePromoteTakePiece
	MoveRulePromoteNoTakePiece

	MoveRuleDataPromoteQueen
	MoveRuleDataPromoteBishop
	MoveRuleDataPromoteRook
	MoveRuleDataPromoteKnight
)

var PIECE_WORTH = map[fen.Piece]int16{
	fen.WhitePawn:   75,
	fen.WhiteKnight: 400,
	fen.WhiteBishop: 600,
	fen.WhiteRook:   900,
	fen.WhiteQueen:  2000,
	fen.WhiteKing:   9000,

	fen.BlackPawn:   75,
	fen.BlackKnight: 400,
	fen.BlackBishop: 600,
	fen.BlackRook:   900,
	fen.BlackQueen:  2000,
	fen.BlackKing:   9000,
}

func RuleSetForType(p fen.Piece) MoveRuleSet {
	switch p {
	case fen.WhitePawn:
		{
			// ToDo: En-passant not properly implemented
			return MoveRuleSet{
				{0, -1, MoveRuleNoTakePiece},
				{1, -1, MoveRuleNeedTakePiece},
				{-1, -1, MoveRuleNeedTakePiece},
				{1, -1, MoveRuleEnPassant},
				{-1, -1, MoveRuleEnPassant},
				{0, -2, MoveRulePawnFirstMove},
				{0, -1, MoveRulePromoteNoTakePiece},
				{1, -1, MoveRulePromoteTakePiece},
				{-1, -1, MoveRulePromoteTakePiece},
			}
		}
	case fen.BlackPawn:
		{
			return MoveRuleSet{
				{0, 1, MoveRuleNoTakePiece},
				{1, 1, MoveRuleNeedTakePiece},
				{-1, 1, MoveRuleNeedTakePiece},
				{1, 1, MoveRuleEnPassant},
				{-1, 1, MoveRuleEnPassant},
				{0, 2, MoveRulePawnFirstMove},
				{0, 1, MoveRulePromoteNoTakePiece},
				{1, 1, MoveRulePromoteTakePiece},
				{-1, 1, MoveRulePromoteTakePiece},
			}
		}
	case fen.BlackQueen,
		fen.WhiteQueen:
		{
			return MoveRuleSet{
				{0, 1, MoveRuleAlongAxis},
				{0, -1, MoveRuleAlongAxis},
				{1, 1, MoveRuleAlongAxis},
				{1, 0, MoveRuleAlongAxis},
				{1, -1, MoveRuleAlongAxis},
				{-1, 1, MoveRuleAlongAxis},
				{-1, 0, MoveRuleAlongAxis},
				{-1, -1, MoveRuleAlongAxis},
			}
		}
	case fen.BlackKing,
		fen.WhiteKing:
		{
			return MoveRuleSet{
				{0, 1, MoveRuleKing2Distance},
				{1, 1, MoveRuleKing2Distance},
				{1, 0, MoveRuleKing2Distance},
				{1, -1, MoveRuleKing2Distance},
				{0, -1, MoveRuleKing2Distance},
				{-1, -1, MoveRuleKing2Distance},
				{-1, 0, MoveRuleKing2Distance},
				{-1, 1, MoveRuleKing2Distance},
				{0, 0, MoveRuleCastleKingside},
				{0, 0, MoveRuleCastleQueenside},
			}
		}
	case fen.BlackRook,
		fen.WhiteRook:
		{
			return MoveRuleSet{
				{1, 0, MoveRuleAlongAxis},
				{-1, 0, MoveRuleAlongAxis},
				{0, 1, MoveRuleAlongAxis},
				{0, -1, MoveRuleAlongAxis},
			}
		}
	case fen.BlackKnight,
		fen.WhiteKnight:
		{
			return MoveRuleSet{
				{1, -2, MoveRuleDefault},
				{-1, -2, MoveRuleDefault},
				{1, 2, MoveRuleDefault},
				{-1, 2, MoveRuleDefault},
				{2, 1, MoveRuleDefault},
				{2, -1, MoveRuleDefault},
				{-2, 1, MoveRuleDefault},
				{-2, -1, MoveRuleDefault},
			}
		}
	case fen.BlackBishop,
		fen.WhiteBishop:
		{
			return MoveRuleSet{
				{1, 1, MoveRuleAlongAxis},
				{1, -1, MoveRuleAlongAxis},
				{-1, 1, MoveRuleAlongAxis},
				{-1, -1, MoveRuleAlongAxis},
			}
		}
	}

	return MoveRuleSet{}
}

func IsRulePromote(rule int8) bool {
	switch rule {
	case MoveRulePromoteTakePiece,
		MoveRulePromoteNoTakePiece,

		MoveRuleDataPromoteQueen,
		MoveRuleDataPromoteBishop,
		MoveRuleDataPromoteRook,
		MoveRuleDataPromoteKnight:
		{

			return true
		}
	}

	return false
}

func IsRuleCastle(rule int8) bool {
	return rule == MoveRuleCastleKingside || rule == MoveRuleCastleQueenside
}
