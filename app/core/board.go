package core

import (
	fen2 "chss/lib/fen"
	"chss/lib/pgn"
)

type Board [64]fen2.Piece

func NewBoard() Board {
	var board Board
	for i := 0; i < 64; i++ {
		board[i] = fen2.NilPiece
	}

	return board
}
func (b Board) PieceAt(x, y int8) fen2.Piece {
	i := x + y*8

	if i < 0 || i > 63 {
		return fen2.NilPiece
	}

	return b[i]
}
func (b Board) Pieces() []Piece {
	var pieces []Piece

	for i := range b {
		if b[i] == fen2.NilPiece {
			continue
		}

		pieces = append(pieces, Piece{byte(b[i]), byte(i % 8), byte(i / 8)})
	}

	return pieces
}

// SetPiece is a helper to easily change a rune in Board and prevent unwanted behaviour.
func (b *Board) SetPiece(x, y int8, piece fen2.Piece) {
	b[x+y*8] = piece
}

func (b Board) TryMove(x, y int8, move [3]int8) bool {
	p := b.PieceAt(x, y)
	if p == fen2.NilPiece {
		return false
	}

	switch move[2] {
	case MoveRulePawnFirstMove:
		{
			if (p.IsBlack() && b.PieceAt(x, y+1) != fen2.NilPiece) || p.IsWhite() && b.PieceAt(x, y-1) != fen2.NilPiece {
				return false
			}
		}
	case MoveRuleNeedTakePiece: // Static move + must take piece
		{
			if b.PieceAt(move[0], move[1]).IsBlack() == p.IsBlack() {
				return false
			}
		}
	case MoveRuleEnPassant: // Pawn En-passant
		{
			if p.IsBlack() {
				t := b.PieceAt(move[0], move[1]+1)
				if !(t != fen2.NilPiece && t.IsBlack() != p.IsBlack()) {
					return false
				}
			} else {
				t := b.PieceAt(move[0], move[1]-1)
				if !(t != fen2.NilPiece && t.IsWhite() != p.IsWhite()) {
					return false
				}
			}
		}
	}

	return true
}

func (b *Board) Move(x, y int8, move [3]int8) bool {

	if ok := b.TryMove(x, y, move); !ok {
		return false
	}

	p := b.PieceAt(x, y)

	switch move[2] {
	case MoveRuleDefault, // normal move, we don't need more sanitization here than BitBoard does for now.
		MoveRulePawnFirstMove,
		MoveRuleAlongAxis,
		MoveRuleNoTakePiece,
		MoveRuleKing2Distance:
		{
			b.move(x, y, move)
		}
	case MoveRuleNeedTakePiece: // Static move + must take piece
		{
			b.move(x, y, move)
		}
	case MoveRuleEnPassant: // Pawn En-passant
		{
			if p.IsBlack() {
				b.move(x, y, move)
				b.SetPiece(move[0], move[1]+1, fen2.NilPiece)
			} else {
				b.move(x, y, move)
				b.SetPiece(move[0], move[1]-1, fen2.NilPiece)
			}
		}
	case MoveRuleCastleKingside: // Castle Kingside, preliminaries done in BitBoard
		{
			if p.IsBlack() {
				b.move(x, y, [3]int8{6, 0, move[2]}) // mv King
				b.move(7, 0, [3]int8{5, 0, move[2]}) // mv Rook
			} else if p.IsWhite() {
				b.move(x, y, [3]int8{6, 7, move[2]}) // mv King
				b.move(7, 7, [3]int8{5, 7, move[2]}) // mv Rook
			}
		}
	case MoveRuleCastleQueenside: // Castle Queenside
		{
			if p.IsBlack() {
				b.move(x, y, [3]int8{2, 0, move[2]}) // mv King
				b.move(0, 0, [3]int8{3, 0, move[2]}) // mv Rook
			} else if p.IsWhite() {
				b.move(x, y, [3]int8{2, 7, move[2]}) // mv King
				b.move(0, 7, [3]int8{3, 7, move[2]}) // mv Rook
			}
		}
	case MoveRuleDataPromoteQueen:
		{
			b.move(x, y, move)
			b.SetPiece(move[0], move[1], pgn.PieceQueen.ToFEN(p.Player()))
		}
	case MoveRuleDataPromoteBishop:
		{
			b.move(x, y, move)
			b.SetPiece(move[0], move[1], pgn.PieceBishop.ToFEN(p.Player()))
		}
	case MoveRuleDataPromoteRook:
		{
			b.move(x, y, move)
			b.SetPiece(move[0], move[1], pgn.PieceRook.ToFEN(p.Player()))
		}
	case MoveRuleDataPromoteKnight:
		{
			b.move(x, y, move)
			b.SetPiece(move[0], move[1], pgn.PieceKnight.ToFEN(p.Player()))
		}
	default:
		{
			return false
		}
	}

	return true
}

func (b *Board) move(x, y int8, move [3]int8) {
	p := b.PieceAt(x, y)

	b.SetPiece(x, y, fen2.NilPiece)
	b.SetPiece(move[0], move[1], p)
}

func (b Board) WhitePiecesCount() int {
	c := 0

	for i := 0; i < len(b); i++ {
		if fen2.Piece(b[i]).IsWhite() {
			c += 1
		}
	}

	return c
}
func (b Board) WhitePiecesWorth() int16 {
	var worth int16

	for i := 0; i < len(b); i++ {
		p := fen2.Piece(b[i])

		if p.IsWhite() {
			worth += PIECE_WORTH[p]
		}
	}

	return worth
}
func (b Board) BlackPiecesCount() int {
	c := 0

	for i := 0; i < len(b); i++ {
		if fen2.Piece(b[i]).IsBlack() {
			c += 1
		}
	}

	return c
}
func (b Board) BlackPiecesWorth() int16 {
	var worth int16

	for i := 0; i < len(b); i++ {
		p := fen2.Piece(b[i])

		if p.IsWhite() {
			worth += PIECE_WORTH[b[i]]
		}
	}

	return worth
}

func (b Board) IsAnyKingDead() bool {
	k1, k2 := false, false

	for i := range b {
		if b[i] == fen2.WhiteKing {
			k1 = true
		} else if b[i] == fen2.BlackKing {
			k2 = true
		}
	}

	return !(k1 && k2)
}

func (b Board) IsWhiteKingDead() bool {
	k1 := false

	for i := range b {
		if b[i] == fen2.WhiteKing {
			k1 = true
		}
	}

	return !k1
}
func IsWhiteKingDead(state [64]fen2.Piece) bool {
	for _, piece := range state {
		if piece == fen2.WhiteKing {
			return true
		}
	}

	return false
}

func (b Board) IsBlackKingDead() bool {
	k2 := false

	for i := range b {
		if b[i] == fen2.BlackKing {
			k2 = true
		}
	}

	return !k2
}
func IsBlackKingDead(state [64]fen2.Piece) bool {
	for _, piece := range state {
		if piece == fen2.BlackKing {
			return true
		}
	}

	return false
}

// FallenPieces loops through State and compiles a list
// of all "missing" pieces.
func (b Board) FallenPieces() []fen2.Piece {
	livePieces := map[fen2.Piece]int32{
		fen2.WhitePawn:   0,
		fen2.WhiteRook:   0,
		fen2.WhiteKnight: 0,
		fen2.WhiteBishop: 0,
		fen2.WhiteQueen:  0,
		fen2.WhiteKing:   0,
		fen2.BlackPawn:   0,
		fen2.BlackRook:   0,
		fen2.BlackKnight: 0,
		fen2.BlackBishop: 0,
		fen2.BlackQueen:  0,
		fen2.BlackKing:   0,
	}
	totalPieces := map[fen2.Piece]int32{
		fen2.WhitePawn:   8,
		fen2.WhiteRook:   2,
		fen2.WhiteKnight: 2,
		fen2.WhiteBishop: 2,
		fen2.WhiteQueen:  1,
		fen2.WhiteKing:   1,
		fen2.BlackPawn:   8,
		fen2.BlackRook:   2,
		fen2.BlackKnight: 2,
		fen2.BlackBishop: 2,
		fen2.BlackQueen:  1,
		fen2.BlackKing:   1,
	}

	for _, p := range b {
		if p == fen2.NilPiece {
			continue
		}
		livePieces[p] += 1
	}

	list := []fen2.Piece{}

	for piece, liveCount := range livePieces {
		for i := int32(0); i < totalPieces[piece]-liveCount; i++ {
			list = append(list, piece)
		}
	}

	return list
}

// FallenWhitePieces same as FallenPieces, but gives only a list
// of white game pieces.
func (b Board) FallenWhitePieces() []fen2.Piece {
	livePieces := map[fen2.Piece]int32{
		fen2.WhitePawn:   0,
		fen2.WhiteRook:   0,
		fen2.WhiteKnight: 0,
		fen2.WhiteBishop: 0,
		fen2.WhiteQueen:  0,
		fen2.WhiteKing:   0,
	}
	totalPieces := map[fen2.Piece]int32{
		fen2.WhitePawn:   8,
		fen2.WhiteRook:   2,
		fen2.WhiteKnight: 2,
		fen2.WhiteBishop: 2,
		fen2.WhiteQueen:  1,
		fen2.WhiteKing:   1,
	}

	for _, p := range b {
		if p.IsBlack() || p == fen2.NilPiece {
			continue
		}

		livePieces[p] += 1
	}

	list := []fen2.Piece{}

	for piece, liveCount := range livePieces {
		for i := int32(0); i < totalPieces[piece]-liveCount; i++ {
			list = append(list, piece)
		}
	}

	return list
}

// FallenBlackPieces same as FallenPieces, but gives only a list
// of black game pieces.
func (b Board) FallenBlackPieces() []fen2.Piece {
	livePieces := map[fen2.Piece]int32{
		fen2.BlackPawn:   0,
		fen2.BlackRook:   0,
		fen2.BlackKnight: 0,
		fen2.BlackBishop: 0,
		fen2.BlackQueen:  0,
		fen2.BlackKing:   0,
	}
	totalPieces := map[fen2.Piece]int32{
		fen2.BlackPawn:   8,
		fen2.BlackRook:   2,
		fen2.BlackKnight: 2,
		fen2.BlackBishop: 2,
		fen2.BlackQueen:  1,
		fen2.BlackKing:   1,
	}

	for _, p := range b {
		if p.IsWhite() || p == fen2.NilPiece {
			continue
		}

		livePieces[p] += 1
	}

	list := []fen2.Piece{}

	for piece, liveCount := range livePieces {
		for i := int32(0); i < totalPieces[piece]-liveCount; i++ {
			list = append(list, piece)
		}
	}

	return list
}

func (b Board) KingPosition(ply fen2.Player) (uint8, uint8, bool) {
	found, kX, kY := false, uint8(0), uint8(0)
	for _, p := range b.Pieces() { // check if enemy threatens our king
		if p.IsAvailableInTurn(ply) && p.Kind().IsKing() {
			found, kX, kY = true, p.X(), p.Y()
		}
	}

	return kX, kY, found
}
