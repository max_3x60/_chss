package app

import "golang.org/x/image/font"

var GameFontSmall font.Face
var GameFontNormal font.Face
var GameFontBig font.Face
var GameFontGiant font.Face
