package app

import (
	"chss/app/core"
	"chss/lib/fen"
	"chss/lib/pgn"
	"fmt"
	"log"
	"math/rand"
)

func SelectMoveFromNodes(moves *core.TreeNode) [5]int8 {
	canThreatenKing, canCapturePiece, canCastle := false, false, false
	mThreat, mCapture, mCastle := [5]int8{}, [5]int8{}, [5]int8{}

	for _, node := range moves.Children {
		var board = node.State

		p := board.PieceAt(node.Move[2], node.Move[3])

		if p == fen.NilPiece {
			continue
		}

		kX, kY, kF := board.KingPosition(p.Player().Opposite())

		// Capture enemy king
		//// Look for a move that put's the enemy king in check.
		bb, _ := core.NewBitBoard(&core.Piece{byte(p), byte(node.Move[2]), byte(node.Move[3])}, board)
		if kF && bb.At(kX, kY) {
			canThreatenKing, mThreat = true, node.Move
			break
		}

		// Castle if possible
		if core.IsRuleCastle(node.Move[4]) {
			canCastle, mCastle = true, node.Move
		}

		// Select highest value move for black side.
		mCapture = moves.Children[moves.MaxWorthChilds[1]].Move
		canCapturePiece = true
	}

	// Check our found moves in priority order
	if canThreatenKing {
		fmt.Println("Can Threaten")

		return mThreat
	} else if canCastle {
		fmt.Println("Can Castle")

		return mCastle
	} else if canCapturePiece {
		fmt.Println("Can Capture")

		return mCapture
	}

	fmt.Println("Random Move")
	i := rand.Intn(len(moves.Children))
	return moves.Children[i].Move
}

func PieceWorth(p fen.Piece) int {
	pP, err := pgn.FromFENPiece(p)
	if err != nil {
		log.Fatal(err)
	}

	switch pP {
	case pgn.PieceKing:
		{
			return P_WORTH_KING
		}
	case pgn.PieceQueen:
		{
			return P_WORTH_QUEEN
		}
	case pgn.PieceBishop:
		{
			return P_WORTH_BISHOP
		}
	case pgn.PieceKnight:
		{
			return P_WORTH_KNIGHT
		}
	case pgn.PieceRook:
		{
			return P_WORTH_ROOK
		}
	case pgn.PiecePawn:
		{
			return P_WORTH_PAWN
		}
	}

	return 0
}
